#Match Wardrobe App
Development Time: 10 Hrs

##Problem Statement
Wardrobe is an app that suggests you combinations of clothes for daily wear. The home screen is divided into two sections, each displaying a shirt (top) and a pair of trousers (bottoms). You can add more shirts or pants by clicking on the ‘+’ button on the right corner of each section. You can also swipe left and right each section if you don’t like the shirt or pants. In the centre, there's a 'shuffle’ button. 

##App Features
- App supports wide range of devices; Views are made in such a way that they will work fine in devices having different screen sizes
- Home screen displays a neat UI and supports both orientation with preserving the state of the app; Alternate resource layout is provided for landscape view
- User can mark any pair favorite and see them all by clicking action bar favorite button
- User can swipe left or right to shuffle any particular image type
- Material vector icons are used to keep app size small and remove resolution problem in different screen density mobiles
- Proguard is also used for release build to make app size small and to make app more secure

##Third Party libraries and References
- Icon image source: Material icons
- [Multipicker](https://github.com/coomar2841/android-multipicker-library) : It was used to pick images from gallery and camera; It is used as Local Module dependency to remove image duplication
- [TapTargetView](https://github.com/KeepSafe/TapTargetView) : To show coach marks to user
- [Sugar](https://github.com/chennaione/sugar) : For SQLiite database; **Disable the Instant run feature of Android Studio before compiling the code; it might not work properly due to this library, read library's github page for more info**
- Stetho Facebook: For debugging sqlite database
