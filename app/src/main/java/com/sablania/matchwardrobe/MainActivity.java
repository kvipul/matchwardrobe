package com.sablania.matchwardrobe;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.facebook.stetho.Stetho;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.kbeanie.multipicker.api.CameraImagePicker;
import com.kbeanie.multipicker.api.ImagePicker;
import com.kbeanie.multipicker.api.Picker;
import com.kbeanie.multipicker.api.callbacks.ImagePickerCallback;
import com.kbeanie.multipicker.api.entity.ChosenImage;
import com.orm.SugarRecord;
import com.sablania.matchwardrobe.models.FavoriteModel;
import com.sablania.matchwardrobe.models.PantModel;
import com.sablania.matchwardrobe.models.ShirtModel;
import com.sablania.matchwardrobe.utils.OnSwipeTouchListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    /**
     * Some older devices needs a small delay between UI widget updates
     * and a change of the status and navigation bar.
     */
    private static final int UI_ANIMATION_DELAY = 300;
    private static final int STORAGE_PERMS = 100;
    private final Handler mHideHandler = new Handler();
    private View mContentView;
    private final Runnable mHidePart2Runnable = new Runnable() {
        @SuppressLint("InlinedApi")
        @Override
        public void run() {
            // Delayed removal of status and navigation bar

            // Note that some of these constants are new as of API 16 (Jelly Bean)
            // and API 19 (KitKat). It is safe to use them, as they are inlined
            // at compile-time and do nothing on earlier devices.
            mContentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        }
    };
    private View mControlsView;
    private final Runnable mShowPart2Runnable = new Runnable() {
        @Override
        public void run() {
            // Delayed display of UI elements
            ActionBar actionBar = getSupportActionBar();
            if (actionBar != null) {
                actionBar.show();
            }
            mControlsView.setVisibility(View.VISIBLE);
        }
    };
    private boolean mVisible;
    private final Runnable mHideRunnable = new Runnable() {
        @Override
        public void run() {
            hide();
        }
    };

    ImageView ivShirt, ivPant;
    FloatingActionButton fabAddShirt, fabAddPant, fabShuffle, fabFavorite, fabFullScreen;
    ImagePicker imagePicker;
    CameraImagePicker cameraImagePicker;
    String shirtPath, pantPath;
    int choseImageType;
    final int SHIRT_TYPE =1 , PANT_TYPE = 2;
    boolean isFavorite = false;
    boolean isFavoriteActivity;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        initialize();
        if(savedInstanceState!=null) {
            shirtPath = savedInstanceState.getString("shirt_path");
            pantPath = savedInstanceState.getString("pant_path");
            setImageToImageView(shirtPath, SHIRT_TYPE);
            setImageToImageView(pantPath, PANT_TYPE);
        }

        // Set up the user interaction to manually show or hide the system UI.
        mContentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggle();
            }
        });

        ivShirt.setOnTouchListener(new OnSwipeTouchListener(this) {
                                       @Override
                                       public void onSwipeLeft() {
                                           super.onSwipeLeft();
                                           setRandomPair(true, false);
                                       }

                                       @Override
                                       public void onSwipeRight() {
                                           super.onSwipeRight();
                                           setRandomPair(true, false);
                                       }
                                   }
        );
        ivPant.setOnTouchListener(new OnSwipeTouchListener(this) {
                                      @Override
                                      public void onSwipeLeft() {
                                          super.onSwipeLeft();
                                          setRandomPair(false, true);
                                      }

                                      @Override
                                      public void onSwipeRight() {
                                          super.onSwipeRight();
                                          setRandomPair(false, true);
                                      }
                                  }
        );

        fabAddShirt.setOnClickListener(this);
        fabAddPant.setOnClickListener(this);
        fabShuffle.setOnClickListener(this);
        fabFavorite.setOnClickListener(this);
        fabFullScreen.setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!isFavoriteActivity) {
            // Inflate the menu; this adds items to the action bar if it is present.
            getMenuInflater().inflate(R.menu.menu_main, menu);
            return true;
        }
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }else if (id == R.id.action_favorite) {
            Intent intent = new Intent(this, MainActivity.class);
            intent.putExtra("favorite", true);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    private void showImageOptions() {
        if (ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            showNeedPermissionsDialog(this);
            return;
        }

        final CharSequence[] items = new CharSequence[]{"From Camera", "From Gallery"};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("From Camera")) {
                    cameraImagePicker.setImagePickerCallback(new ImagePickerCallback(){
                                                                 @Override
                                                                 public void onImagesChosen(List<ChosenImage> images) {
                                                                     setImageToImageView(images.get(0).getOriginalPath(), choseImageType);
                                                                 }
                                                                 @Override
                                                                 public void onError(String message) {
                                                                 }
                                                             }
                    );
                    cameraImagePicker.shouldGenerateMetadata(false); // Default is true
                    cameraImagePicker.shouldGenerateThumbnails(false); // Default is true
                    String outputPath = cameraImagePicker.pickImage();
                } else if (items[item].equals("From Gallery")) {
                    imagePicker.setImagePickerCallback(new ImagePickerCallback(){
                                                           @Override
                                                           public void onImagesChosen(List<ChosenImage> images) {
                                                               if (images.get(0).getOriginalPath2() != null) {
                                                                   setImageToImageView(images.get(0).getOriginalPath2(), choseImageType);
                                                               } else {
                                                                   setImageToImageView(images.get(0).getOriginalPath(), choseImageType);
                                                               }
                                                           }
                                                           @Override
                                                           public void onError(String message) {
                                                           }
                                                       }
                    );
//                    Bundle bundle = new Bundle();
//                    bundle.putInt(MediaStore.EXTRA_SIZE_LIMIT, 300);
                    imagePicker.shouldGenerateMetadata(false); // Default is true
                    imagePicker.shouldGenerateThumbnails(false); // Default is true
                    imagePicker.pickImage();
                }
            }
        });
        builder.show();
    }

    private void setImageToImageView(String imagePath, int imageType) {
        if (imagePath == null) {
            return;
        }
        Bitmap bitmap = BitmapFactory.decodeFile(imagePath);
        if (bitmap == null) {
            Toast.makeText(this, getString(R.string.toast_could_not_read_image), Toast.LENGTH_LONG).show();
        } else {
            if (imageType == SHIRT_TYPE) {
                shirtPath = imagePath;
                ivShirt.setImageBitmap(bitmap);
                SugarRecord.save(new ShirtModel(shirtPath));
            } else {
                pantPath = imagePath;
                ivPant.setImageBitmap(bitmap);
                SugarRecord.save(new PantModel(pantPath));
            }
            setFavorite();
        }
    }

    public  void showNeedPermissionsDialog(final Activity activity) {
        AlertDialog.Builder alert = new AlertDialog.Builder(activity);
        alert.setMessage(activity.getString(R.string.storage_rationale));
        alert.setCancelable(true);
        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                ActivityCompat.requestPermissions(activity,
                        new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        STORAGE_PERMS);
            }
        });
        AlertDialog alertDialog = alert.create();
        alertDialog.show();
    }


    private void initialize() {
        mVisible = true;
        mControlsView = findViewById(R.id.fullscreen_content_controls);
        mContentView = findViewById(R.id.fullscreen_content);
        ivShirt = findViewById(R.id.iv_shirt);
        ivPant = findViewById(R.id.iv_pant);
        fabAddShirt = findViewById(R.id.fab_add_shirt);
        fabAddPant = findViewById(R.id.fab_add_pant);
        fabFavorite = findViewById(R.id.fab_favorite);
        fabShuffle = findViewById(R.id.fab_shuffle);
        fabFullScreen = findViewById(R.id.fab_full_screen);
        imagePicker = new ImagePicker(this);
        cameraImagePicker = new CameraImagePicker(this);
        Stetho.initializeWithDefaults(this);

        isFavoriteActivity = getIntent().getBooleanExtra("favorite", false);
        if (isFavoriteActivity) {
            fabAddPant.setVisibility(View.GONE);
            fabAddShirt.setVisibility(View.GONE);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            setTitle("Favorites");
        }
        SharedPreferences sharedPreferences = getSharedPreferences("Coachmarks", 0);
        if (sharedPreferences.getBoolean("show_coachmarks", true)) {
            new TapTargetSequence(this)
                    .targets(
                            TapTargetforView(fabAddShirt, "Add Shirt/Top", "Click this button to add new shirt/top image from camera or gallery"),
                            TapTargetforView(fabAddPant, "Add Pant", "Click this button to add new pant image from camera or gallery"),
                            TapTargetforView(fabFavorite, "Favorite", "Click this button to mark shown pair as favorite"),
                            TapTargetforView(fabShuffle, "See random pair", "Click this button to see random pair with previously added images"),
                            TapTargetforView(fabFullScreen, "Full screen mode", "Click this button to enable or disable full screen mode")
                    )
                    .considerOuterCircleCanceled(false)
                    .continueOnCancel(true)
                    .start();
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putBoolean("show_coachmarks", false);
            editor.apply();
        }
    }

    public static TapTarget TapTargetforView(View view, String titleText, String descriptionText){
        return TapTarget.forView(view, titleText, descriptionText)
                .titleTextSize(20)                  // Specify the size (in sp) of the title text
                .descriptionTextSize(16)            // Specify the size (in sp) of the description text
                .textTypeface(Typeface.SANS_SERIF)  // Specify a typeface for the text
                .drawShadow(true)                   // Whether to draw a drop shadow or not
                .tintTarget(true)                   // Whether to tint the target view's color
                .transparentTarget(true)           // Specify whether the target is transparent (displays the content underneath)
                ;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK) {
            try {
                if (requestCode == Picker.PICK_IMAGE_CAMERA && cameraImagePicker != null) {
                    cameraImagePicker.submit(data);
                } else if (requestCode == Picker.PICK_IMAGE_DEVICE && imagePicker != null) {
                    imagePicker.submit(data);
                } else {
                    Toast.makeText(this, getString(R.string.toast_could_not_read_image), Toast.LENGTH_LONG).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.e("permission", "granted not granted = "+requestCode);
        if (grantResults.length > 0
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            showImageOptions();
        }
    }


    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        // Trigger the initial hide() shortly after the activity has been
        // created, to briefly hint to the user that UI controls
        // are available.
        delayedHide(100);
    }

    private void toggle() {
        if (mVisible) {
            hide();
        } else {
            show();
        }
    }

    private void hide() {
        // Hide UI first
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
        mControlsView.setVisibility(View.GONE);
        mVisible = false;

        // Schedule a runnable to remove the status and navigation bar after a delay
        mHideHandler.removeCallbacks(mShowPart2Runnable);
        mHideHandler.postDelayed(mHidePart2Runnable, UI_ANIMATION_DELAY);
    }

    @SuppressLint("InlinedApi")
    private void show() {
        // Show the system bar
        mContentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);
        mVisible = true;

        // Schedule a runnable to display UI elements after a delay
        mHideHandler.removeCallbacks(mHidePart2Runnable);
        mHideHandler.postDelayed(mShowPart2Runnable, UI_ANIMATION_DELAY);
    }

    /**
     * Schedules a call to hide() in delay milliseconds, canceling any
     * previously scheduled calls.
     */
    private void delayedHide(int delayMillis) {
        mHideHandler.removeCallbacks(mHideRunnable);
        mHideHandler.postDelayed(mHideRunnable, delayMillis);
    }

    private void setFavorite(){
        if (shirtPath == null || pantPath == null) {
            isFavorite = false;
            fabFavorite.setImageResource(R.drawable.ic_favorite_border);
            return;
        }
        List<FavoriteModel> list = SugarRecord.findWithQuery(FavoriteModel.class, "select * from favorite_model where shirt_path = ? and pant_path = ?", new String[]{shirtPath, pantPath});
        isFavorite = !list.isEmpty();
        if (isFavorite) {
            fabFavorite.setImageResource(R.drawable.ic_favorite);
        } else {
            fabFavorite.setImageResource(R.drawable.ic_favorite_border);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fab_add_shirt:
                choseImageType = SHIRT_TYPE;
                showImageOptions();
                break;
            case R.id.fab_add_pant:
                choseImageType = PANT_TYPE;
                showImageOptions();
                break;
            case R.id.fab_shuffle:
                setRandomPair(true, true);
                break;
            case R.id.fab_favorite:
                if (shirtPath == null || pantPath == null) {
                    Toast.makeText(this, "Please chose images first", Toast.LENGTH_SHORT).show();
                } else {
                    if (isFavorite) {
                        fabFavorite.setImageResource(R.drawable.ic_favorite_border);
                        SugarRecord.deleteAll(FavoriteModel.class, "shirt_path = ?  and pant_path = ?", new String[]{shirtPath, pantPath});
                        Toast.makeText(this, "Pair removed from favorites", Toast.LENGTH_SHORT).show();
                    } else {
                        SugarRecord.save(new FavoriteModel(shirtPath, pantPath));
                        Toast.makeText(this, "Pair marked as favorite", Toast.LENGTH_SHORT).show();
                        fabFavorite.setImageResource(R.drawable.ic_favorite);

                    }
                    isFavorite = !isFavorite;
                }
                break;
            case R.id.fab_full_screen:
                toggle();
                break;
        }
    }

    private void setRandomPair(boolean randomizeShirt, boolean randomizePant) {
        if(isFavoriteActivity){
            List<FavoriteModel> favoriteModels = SugarRecord.listAll(FavoriteModel.class);
            if (favoriteModels.isEmpty()) {
                ivShirt.setImageDrawable(getResources().getDrawable(R.drawable.ic_image));
                ivPant.setImageDrawable(getResources().getDrawable(R.drawable.ic_image));
                Toast.makeText(this, "You don't have any favorite", Toast.LENGTH_SHORT).show();
                return;
            }
            FavoriteModel randomFavorite = favoriteModels.get(new Random().nextInt(favoriteModels.size()));
            if(favoriteModels.size()!=1) {
                if (shirtPath != null && shirtPath.equals(randomFavorite.getShirtPath()) && pantPath != null && pantPath.equals(randomFavorite.getPantPath())) {
                    setRandomPair(true, true);
                    return;
                }
            }
            shirtPath = randomFavorite.getShirtPath();
            pantPath = randomFavorite.getPantPath();
            setImageToImageView(shirtPath, SHIRT_TYPE);
            setImageToImageView(pantPath, PANT_TYPE);
            return;
        }
        List<ShirtModel> shirtModels = new ArrayList<>();
        List<PantModel> pantModels = new ArrayList<>();
        if (randomizeShirt && !randomizePant) {
            shirtModels.addAll(SugarRecord.listAll(ShirtModel.class));
            if (shirtModels.size()>1) {
                int rand = new Random().nextInt(shirtModels.size());
                if(shirtPath!=null && shirtPath.equals(shirtModels.get(rand).getImagePath())) {
                    setRandomPair(true, false);
                }else {
                    shirtPath = shirtModels.get(rand).getImagePath();
                    setImageToImageView(shirtPath, SHIRT_TYPE);
                }
            }
        } else if (!randomizeShirt && randomizePant) {
            pantModels.addAll(SugarRecord.listAll(PantModel.class, "image_path"));
            if (pantModels.size()>1) {
                int rand = new Random().nextInt(pantModels.size());
                if(pantPath!=null && pantPath.equals(pantModels.get(rand).getImagePath())) {
                    setRandomPair(false, true);
                }else {
                    pantPath = pantModels.get(rand).getImagePath();
                    setImageToImageView(pantPath, PANT_TYPE);
                }
            }
        } else if(randomizeShirt && randomizePant) {
            shirtModels.addAll(SugarRecord.listAll(ShirtModel.class));
            pantModels.addAll(SugarRecord.listAll(PantModel.class));
            if (shirtModels.isEmpty() || pantModels.isEmpty() || (shirtModels.size() < 2 && pantModels.size() < 2)) {
                Toast.makeText(this, "You don't have enough image to shuffle, Please add more images!", Toast.LENGTH_SHORT).show();
                return;
            }
            int rand1 = new Random().nextInt(shirtModels.size());
            int rand2 = new Random().nextInt(pantModels.size());
            if (shirtPath!=null && shirtPath.equals(shirtModels.get(rand1).getImagePath()) && pantPath!=null && pantPath.equals(pantModels.get(rand2).getImagePath())) {
                setRandomPair(true, true);
                return;
            }
            shirtPath = shirtModels.get(rand1).getImagePath();
            pantPath = pantModels.get(rand2).getImagePath();
            setImageToImageView(shirtPath, SHIRT_TYPE);
            setImageToImageView(pantPath, PANT_TYPE);
        }
    }

    @Override
    protected void onSaveInstanceState (Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("shirt_path", shirtPath);
        outState.putString("pant_path", pantPath);
    }
}
