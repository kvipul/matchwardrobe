package com.sablania.matchwardrobe.application;

import android.app.Application;

import com.orm.SugarContext;

/**
 * Created by ViPul Sublaniya on 20-05-2018.
 */

public class MatchWardrobe extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        SugarContext.init(this);

    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        SugarContext.terminate();

    }
}
