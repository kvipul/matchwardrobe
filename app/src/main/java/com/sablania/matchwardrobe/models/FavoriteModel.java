package com.sablania.matchwardrobe.models;

import com.google.gson.annotations.SerializedName;
import com.orm.dsl.Table;
import com.orm.dsl.Unique;

import java.io.Serializable;

/**
 * Created by ViPul Sublaniya on 20-05-2018.
 */
@Table
public class FavoriteModel implements Serializable {
    @SerializedName("shirt_path")
    private String shirtPath;

    @SerializedName("pant_path")
    private String pantPath;

    public FavoriteModel(String shirtPath, String pantPath) {
        this.shirtPath = shirtPath;
        this.pantPath = pantPath;
    }

    public FavoriteModel() {
    }

    public String getShirtPath() {
        return shirtPath;
    }

    public void setShirtPath(String shirtPath) {
        this.shirtPath = shirtPath;
    }

    public String getPantPath() {
        return pantPath;
    }

    public void setPantPath(String pantPath) {
        this.pantPath = pantPath;
    }
}


