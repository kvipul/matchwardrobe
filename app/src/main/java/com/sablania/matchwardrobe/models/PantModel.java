package com.sablania.matchwardrobe.models;

import com.google.gson.annotations.SerializedName;
import com.orm.dsl.Table;
import com.orm.dsl.Unique;

import java.io.Serializable;

/**
 * Created by ViPul Sublaniya on 20-05-2018.
 */
@Table
public class PantModel implements Serializable {
    @Unique
    @SerializedName("image_path")
    private String imagePath;

    @SerializedName("name")
    private String name;

    public PantModel() {
    }

    public PantModel(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
